# Whiskers project 🐈

## Purpose of this project:
We want to help the local animal shelters by making an app which connects them with the people who want to adopt

## Concept:
The app is based on Tinder. After registration people can start swipping animals. The animals have a profile picture as well as some information about their nature (about their temper, which food they prefer, etc.)

## Current state:
 - card swipe feature added
 - ~~login added~~ (see at Issues)