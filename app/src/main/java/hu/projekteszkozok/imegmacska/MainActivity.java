package hu.projekteszkozok.imegmacska;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    private Firebase mRef;
    private Random rand;
    private DatabaseReference mDatabase;
    private String actualUrl;
    private String actualId;
    private String actualLink = "";
    private String actualAvailable = "";
    private String actualReference = "";
    private ArrayList<String> links = new ArrayList<>();
    private ArrayList<String> references = new ArrayList<>();

    private Button mLike, mDislike, mSignOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        rand = new Random();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        links.add("https://imegmacska.firebaseio.com/cat1");
        links.add("https://imegmacska.firebaseio.com/cat2");
        links.add("https://imegmacska.firebaseio.com/cat3");
        links.add("https://imegmacska.firebaseio.com/cat4");
        links.add("https://imegmacska.firebaseio.com/cat5");
        links.add("https://imegmacska.firebaseio.com/cat6");
        links.add("https://imegmacska.firebaseio.com/cat7");
        links.add("https://imegmacska.firebaseio.com/cat8");
        links.add("https://imegmacska.firebaseio.com/cat9");
        links.add("https://imegmacska.firebaseio.com/cat10");
        references.add("cat1");
        references.add("cat2");
        references.add("cat3");
        references.add("cat4");
        references.add("cat5");
        references.add("cat6");
        references.add("cat7");
        references.add("cat8");
        references.add("cat9");
        references.add("cat10");


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Firebase.setAndroidContext(this);

        getCat();

        mLike = findViewById(R.id.like);
        mDislike = findViewById(R.id.dislike);
        mSignOut = findViewById(R.id.sign_out);
        imageView = findViewById(R.id.img_field);

        mLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeCat(actualReference, actualUrl, actualId, "1");

                Intent intent = new Intent(MainActivity.this, ReservationActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCat();
            }
        });

        mSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAuth.getInstance().signOut();

                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void getCat(){

        int random = rand.nextInt(10);
        actualLink = links.get(random);
        actualReference = references.get(random);
        mRef = new Firebase(actualLink);
        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Map<String, String> map = dataSnapshot.getValue(Map.class);
                actualUrl = map.get("url");
                actualId = map.get("id");
                actualAvailable = map.get("available");
                if(!actualAvailable.equals("0")){
                    getCat();
                }
                Picasso.get().load(map.get("url")).into(imageView);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    private void writeCat(String link, String url, String id, String available){
        Cat cat = new Cat(url, id, available);
        mDatabase.child(link).setValue(cat);
    }




}
