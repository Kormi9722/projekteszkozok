package hu.projekteszkozok.imegmacska;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;

public class ReservationActivity extends AppCompatActivity {


    private DatabaseReference mDatabase;
    private ArrayList<String> links = new ArrayList<>();
    private ArrayList<String> references = new ArrayList<>();
    private String actualLink = "";
    private String actualReference = "";
    private Firebase mRef;
    private int found = 0;
    private String actualUrl = "";
    private String actualId = "";
    private String actualAvailable = "";
    private EditText mPhone;
    private Button mReserve;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);

        links.add("https://imegmacska.firebaseio.com/cat1");
        links.add("https://imegmacska.firebaseio.com/cat2");
        links.add("https://imegmacska.firebaseio.com/cat3");
        links.add("https://imegmacska.firebaseio.com/cat4");
        links.add("https://imegmacska.firebaseio.com/cat5");
        links.add("https://imegmacska.firebaseio.com/cat6");
        links.add("https://imegmacska.firebaseio.com/cat7");
        links.add("https://imegmacska.firebaseio.com/cat8");
        links.add("https://imegmacska.firebaseio.com/cat9");
        links.add("https://imegmacska.firebaseio.com/cat10");
        references.add("cat1");
        references.add("cat2");
        references.add("cat3");
        references.add("cat4");
        references.add("cat5");
        references.add("cat6");
        references.add("cat7");
        references.add("cat8");
        references.add("cat9");
        references.add("cat10");


        mPhone = (EditText) findViewById(R.id.phone);

        mReserve = findViewById(R.id.reserve);

        getNext(found);

        mReserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = mPhone.getText().toString();

                if (TextUtils.isEmpty(phoneNumber)) {
                    Toast.makeText(ReservationActivity.this, "Hianyzo adat", Toast.LENGTH_LONG).show();
                    return;
                }

                if (phoneNumber.length() < 6) {
                    Toast.makeText(ReservationActivity.this, "A telefonszám túl rövid", Toast.LENGTH_LONG).show();
                    return;
                }

                //writeCat(references.get(found - 1), actualUrl, actualId, phoneNumber);
                Toast.makeText(ReservationActivity.this, "Sikeres foglalás", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ReservationActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    private void getNext(final int i){

        actualLink = links.get(i);
        actualReference = references.get(i);
        mRef = new Firebase(actualLink);
        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Map<String, String> map = dataSnapshot.getValue(Map.class);
                actualUrl = map.get("url");
                actualId = map.get("id");
                actualAvailable = map.get("available");
                if(actualAvailable.equals("1")){
                    found = i;
                    return;
                }
                getNext(i + 1);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

}
