package hu.projekteszkozok.imegmacska;

public class Cat {

    public String available;
    public String id;
    public String url;

    public Cat(){

    }

    public Cat(String url, String id, String available){
        this.available = available;
        this.id = id;
        this.url = url;
    }

}
