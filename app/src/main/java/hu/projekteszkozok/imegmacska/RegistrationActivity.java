package hu.projekteszkozok.imegmacska;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegistrationActivity extends AppCompatActivity {

    private Button mRegister;
    private EditText mUsername, mPassword, mPasswordAgain, mEmail;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        mUsername = (EditText) findViewById(R.id.r_username);
        mPassword = (EditText) findViewById(R.id.r_password);
        mPasswordAgain = (EditText) findViewById(R.id.r_password_again);
        mEmail = (EditText) findViewById(R.id.r_email);

        mAuth = FirebaseAuth.getInstance();

        mRegister = findViewById(R.id.sign_up);

        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = mEmail.getText().toString();
                String password = mPassword.getText().toString();
                String passwordAgain = mPasswordAgain.getText().toString();
                String username = mUsername.getText().toString();

                if(TextUtils.isEmpty(email) || TextUtils.isEmpty(password) || TextUtils.isEmpty(passwordAgain) || TextUtils.isEmpty(username)){
                    Toast.makeText(RegistrationActivity.this, "Hianyzo adat", Toast.LENGTH_LONG).show();
                    return;
                }

                if(!password.equals(passwordAgain)){
                    Toast.makeText(RegistrationActivity.this, "A ket jelszo nem egyezik", Toast.LENGTH_LONG).show();
                    return;
                }

                if(password.length() < 6){
                    Toast.makeText(RegistrationActivity.this, "A jelszonak legalabb 6 karakternek kell lennie", Toast.LENGTH_LONG).show();
                    return;
                }

                mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(RegistrationActivity.this, "Sikeres regisztráció", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(RegistrationActivity.this, "Hiba a regisztráció során", Toast.LENGTH_LONG).show();
                            }
                    }
                });
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
